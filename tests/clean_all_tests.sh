#!/bin/bash
for d in */ ; do 
	UNIT_NAME=$( sed -En 's/SUPSERV_UNIT_NAME=(.*)/\1/p' ${d}/.env )
	if [[ -z ${UNIT_NAME} ]] ; then UNIT_NAME="superserver" ; fi
	/etc/${UNIT_NAME}/clean
done
