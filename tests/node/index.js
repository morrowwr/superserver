const app  = require( 'express' )();
const id   = process.env.SUPSERV_PROC_ID;
const port = process.env.SUPSERV_PROC_PORT;
const path = process.env.SUPSERV_UNIT_NAME;
app.get( '/*' , (req, res) => res.send( `[NODE/EXPRESS] Hello World! Process ${id} listening on port ${port} got call to ${req.url}\n` ) );
app.listen( port , () => console.log( `Test app listening on port ${port}!` ) )