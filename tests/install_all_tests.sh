#!/bin/bash
for d in ???*/ ; do 
	printf "\n\nINSTALLING TESTS IN ${d}\n\n"
	${SUPSERV_SUPSERV_DIR}/superserver -n 3 --env-var-file ${d}/.env --source-root ${d}
	if [[ $? -eq 0 ]] ; then 
		UNIT_NAME=$( sed -En 's/SUPSERV_UNIT_NAME=(.*)/\1/p' ${d}/.env )
		if [[ -z ${UNIT_NAME} ]] ; then UNIT_NAME="superserver" ; fi
		systemctl restart ${UNIT_NAME}.service
	fi 
done