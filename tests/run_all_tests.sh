#!/bin/bash
for d in ???*/ ; do 
	cat "${d}/.env"
	UNIT_NAME=$( sed -En 's/SUPSERV_UNIT_NAME=(.*)/\1/p' ${d}/.env )
	if [[ -z ${UNIT_NAME} ]] ; then UNIT_NAME="superserver" ; fi
	for i in $( seq 1 10 ) ; do curl http://localhost/${UNIT_NAME}/path ; done
done
