#!/bin/bash
if [[ $( which go | wc -l ) -eq 0 ]] ; then
	echo "This example requires that golang is installed and set up." >&2
	exit 1
fi
if [[ -z ${GOPATH} ]] ; then
	echo "Couldn't find GOPATH in your environment; have you configured golang?" 
	exit 1
fi