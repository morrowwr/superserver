package main

import ( "os" ; "fmt" ; "log" ; "net/http" )

func handler( w http.ResponseWriter , r *http.Request ) {
    fmt.Fprintf( w , "[GOLANG] Hello World! Process %s listening on port %s got call to %s\n" , 
    				os.Getenv( "SUPSERV_PROC_ID" ) , 
    				os.Getenv( "SUPSERV_PROC_PORT" ) , 
    				r.URL.Path )
}

func main() {
    http.HandleFunc( "/" , handler )
    fmt.Printf( "Starting process %s server at %s\n" , os.Getenv( "SUPSERV_PROC_ID" ) , os.Getenv( "SUPSERV_PROC_PORT" ) )
    log.Fatal( http.ListenAndServe( ":" + os.Getenv("SUPSERV_PROC_PORT") , nil ) )
}