#!flask/bin/python
from flask import Flask
from flask import request
import argparse

app , port , id = Flask( __name__ ) , 5000 , None

# Flask catch-all URL from 
# 
# 		http://flask.pocoo.org/snippets/57/
# 
@app.route('/', defaults={ 'path' : '' } )
@app.route('/<path:path>')
def route( path ) :
	return '[PYTHON/FLASK] Hello World! Process %s listening on port %i got call to %s\n' % ( '-' if id is None else str(id) , port , request.url )
   
if __name__ == '__main__' :
	parser = argparse.ArgumentParser()
	parser.add_argument( '-p' , '--port' , default=5000 , help="port to run server on" , type=int )
	parser.add_argument( '-i' , '--id' , default=None , help="" , type=int )
	args = parser.parse_args()
	print( args.port )
	port , id = int(args.port) , args.id
	app.run( port=args.port , host='0.0.0.0' )
