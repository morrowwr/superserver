# superserver

`superserver` is a script that works in concert with [`systemd`](https://www.freedesktop.org/wiki/Software/systemd/) to make load-balanced thread-scaled stateless server setup easy. "installing" will create and install a number of `systemd` services that call the same server code (that you provide), but runs them listening on different ports, load balances requests using [`apache`](https://httpd.apache.org/), and uses `systemd` for a degree of fault tolerance in the separate processes. 

## Dependencies

This code is currently for Linux systems with `systemd` running the `apache` webserver. Tested on Ubuntu 16.04. 

You need to supply the underlying server to load balance, of course. We provide straightforward [examples](#tests) in `node.js`, `python`, and `golang` for testing purposes. 

# Usage

All our instructions currently presume you have cloned this repository. Where it is important to run a command from a particular directory, we prepend the shell CLI `$` with the appropriate path. If there is no path, the intention is to show that the command can be run from anywhere. 

You can get command help by running `./superserver --help` or `./superserver -h` as is common: 

```
superserver$ ./superserver --help
usage: clargs.py [-h] [-e ENV_VAR_FILE] [-u UNIT_NAME] [-d HOME_DIR]
                 [-l LOG_DIR] [-P BASE_PORT] [-n NUM_PROCS] [-b BALANCER]
                 [-p LOCATION] [-w APACHE_CONF] [-r SOURCE_ROOT] [-s SOURCE]
                 [-a ARCHIVE] [-i INSTALL] [-x START_SCRIPT] [-k STOP_SCRIPT]
                 [-t] [-A] [-q]

optional arguments:
  -h, --help            show this help message and exit
  -e ENV_VAR_FILE, --env-var-file ENV_VAR_FILE
  -u UNIT_NAME, --unit-name UNIT_NAME
  -d HOME_DIR, --home-dir HOME_DIR
  -l LOG_DIR, --log-dir LOG_DIR
  -P BASE_PORT, --base-port BASE_PORT
  -n NUM_PROCS, --num-procs NUM_PROCS
  -b BALANCER, --balancer BALANCER
  -p LOCATION, --location LOCATION
  -w APACHE_CONF, --apache-conf APACHE_CONF
  -r SOURCE_ROOT, --source-root SOURCE_ROOT
  -s SOURCE, --source SOURCE
  -a ARCHIVE, --archive ARCHIVE
  -i INSTALL, --install INSTALL
  -x START_SCRIPT, --start-script START_SCRIPT
  -k STOP_SCRIPT, --stop-script STOP_SCRIPT
  -t, --dry-run
  -A, --keep-archive
  -q, --quiet
```

Each of these "metavars" (e.g., `UNIT_NAME`) can be used from your environment (or an environment file) if prepended with "`SUPSERV_`". This is further elaborated on below. 

## Simplest Case

The simplest usage is: 

```
superserver$ sudo ./superserver
```

Note though that you don't have a `start.sh` file in the current folder this will crash: 

```
= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

  SUPERSERVER v0.1

= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

  Installing superserver: a wrapper service for simple apache load  
  balancing of other servers you want to run on your machine. 

  ...

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

Sorry, must have a valid start script (otherwise I can't know what to do).
```

Let's make a trivial `start.sh` file: 
```
#!/bin/bash
echo "Hello world from process ${SUPSERV_PROC_ID} on port ${SUPSERV_PROC_PORT}!"
sleep infinity
```

Then, since you do have a `start.sh` file in the current directory, running

```
superserver$ sudo ./superserver
```

creates two `systemd` services: `superserver` and `superserver-1`. Starting (stopping) `superserver-1` just wraps a call to start (stop) your desired server process, listening on port `5001`, in the background. `superserver` is a "`oneshot`" service that just starts (stops) `superserver-1` when started (stopped). If there is an editable section in the `apache` config, this will get updated as well; if there is not, the script prints out a conf snippet your you to include yourself. 

This command does _not_ start the processes, nor does it restart `apache`. You can "check" the results with 

```
$ /etc/superserver/list
-rw-r--r-- 1 root root 509 Jun  9 13:44 /etc/systemd/system/superserver-1.service
-rw-r--r-- 1 root root 433 Jun  9 13:44 /etc/systemd/system/superserver.service
$ /etc/superserver/check
superserver/superserver: 0/1 process services currently active.
```

You have to explicitly start with

```
$ sudo /etc/superserver/start
```

which is just a wrapper of 

```
$ systemctl restart superserver.service
```

You can "check" that the process is now running with 

```
$ /etc/superserver/check
superserver/superserver: 1/1 process services currently active.
```

If you want to check the output we coded in, run 

```
$ cat /var/log/superserver/1/*.log
Hello world from process 1 on port 5001!
```

If there are running processes, you can stop them with

```
$ sudo /etc/superserver/stop
```

which is just a wrapper of 

```
$ systemctl stop superserver.service
```

When you need to clean up, you can run 

```
$ sudo /etc/superserver/remove
```

to stop and remove these `systemd` services created by `install.sh`, and

```
$ sudo /etc/superserver/clean
```

to call `remove` and _also_ delete all the files and logs created by the install. Make sure you at least `stop` this example to not leave a sleeping process running on your system. 

## Scaling your Server

The simplest _nontrivial_ usage is something like:

```
superserver$ sudo ./superserver -n 4
```

This does the same as above, but now creates **five** `systemd` services: `superserver` and `superserver-1`, `superserver-2`, `superserver-3`, and `superserver-4`. Starting (stopping) `superserver-${n}` just wraps a call to start (stop) your desired server process, listening on port `500${n}`, in the background, while starting (stopping) `superserver` starts (stops) them all. The install also either prints out an `apache` configuration snippet to load balance requests to `localhost/superserver` across the services listening on ports `5001`-`5004` or, if it can find a similar section already, will just replace it directly. 

Starting and checking now yields: 

```
$ sudo /etc/superserver/start
$ cat /var/log/superserver/*/*.log
Hello world from process 1 on port 5001!
Hello world from process 2 on port 5002!
Hello world from process 3 on port 5003!
Hello world from process 4 on port 5004!
```

Make sure you stop and clean up with 

```
$ sudo /etc/superserver/clean
```

This example illustrates the main purpose behind `superserver`: to make it (relatively) easy to scale out custom servers you might get or build. 

## Using .env

Running the above uses a bunch of defaults. What defaults, and their variables, are defined in `.defaults` and discussed below, in [Options](#options). 

If you don't want to use the defaults, you can use `.env` files. We've included a file, `tests/node/.env.full`, that defines several environment variables sufficient to install our `node.js` test as an example. This file is simply

```
SUPSERV_UNIT_NAME=basic-node-test
SUPSERV_SOURCE_ROOT=${SUPSERV_SUPSERV_ROOT}/tests/node
SUPSERV_SOURCE="index.js package.json"
```

and you can install with 

```
superserver$ sudo ./superserver --env-var-file tests/node/.env.full
```

## Using Command Line Arguments

You can also use command line arguments to supersede defaults. To install our `node.js` test, you can do: 

```
superserver$ sudo ./superserver \
      --unit-name basic-node-test \
      --source-root tests/node \
      --source "index.js package.json" \
      --num-procs 4
```

## superserver Core

The `superserver` script needs to know where the "core" (`.defaults` and `clargs.py`) is to run correctly. The assumption is that this is your current directory, but this may not be so. For example, I installed `superserver` in my `${HOME}` dir and get this failure trying to install from `tests`: 

```
superserver/tests$ sudo ./../superserver --env-var-file node/.env.full

= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

  SUPERSERVER v0.1

= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

  Installing superserver: a wrapper service for simple apache load  
  balancing of other servers you want to run on your machine.

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

  Sorry, does not look like

    /home/${USER}/superserver/tests

  is the right directory. Make sure you set "SUPSERV_SUPSERV_DIR".

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
```

You can correct this by setting `SUPSERV_SUPSERV_DIR`: 

```
superserver/tests$ sudo SUPSERV_SUPSERV_DIR=../ ./../superserver --env-var-file node/.env.full
...
```

Note that we _can't use a command-line argument_, because we need this address to find `clargs.py` which _parses_ the command line arguments. 

# Options

Install options are defined by (in order): defaults, values in any `.env` file, values in the current environment, and finally any command-line arguments passed. You can specify the right `.env` file to use in a variable in the current environment or in a command line argument. 

## env variables and file

The `superserver` script uses variables in your environment, or specified in a `.env` file, defined as follows (defaults in "(` `)"): 

* `SUPSERV_ENV_VAR_FILE`: The `.env` file to use, if any (`.env`)
* `SUPSERV_UNIT_NAME`: The actual name to assign to the service (`superserver`)
* `SUPSERV_HOME_DIR`: The location to store executables, scripts, and data at (`/etc/superserver`)
* `SUPSERV_LOG_DIR`: The location to store log files at (`/var/log/superserver`)
* `SUPSERV_BALANCER`: The name of the load balancer to use within the `apache` config (`superserver`)
* `SUPSERV_LOCATION`: The webpath at your host you want to get responses from (`superserver`)
* `SUPSERV_APACHE_CONF`: The `apache` config file to modify, if possible, with the load balancer definition (`/etc/apache2/apache.conf`)
* `SUPSERV_BASE_PORT`: The base port to run processes from (`5000`)
* `SUPSERV_NUM_PROC`: The number of processes to run concurrently and balance over (`1`)
* `SUPSERV_SOURCE_ROOT`: A shared root path for all source files (`.`)
* `SUPSERV_SOURCE`: A string (within quotations if you need spaces) describing source files to bundle together (`""`)
* `SUPSERV_ARCHIVE`: A (`tgz`) archive which, when unpacked, is the working directory for any process (`archive.tgz`)
* `SUPSERV_INSTALL`: An (optional) install script to run in the working directory (`install.sh`)
* `SUPSERV_START_SCRIPT`: The (relative or full) path to the script to use when starting processes (`start.sh`)
* `SUPSERV_STOP_SCRIPT`: The (optional, relative or full) path to the script to use when stopping processes (`stop.sh`)
* `SUPSERV_KEEP_ARCHIVE`: Keep any archive made from bundling source files (`false`)
* `SUPSERV_DRY_RUN`: Just parse the configuration for an install, print it out, and exit (`false`)
* `SUPSERV_QUIET`: Suppress informative prints from an install (`false`)

## Command-Line Options

We allow overwrites with command-line options, using a simple `python` script `clargs.py` that wraps [`argparse`](https://docs.python.org/3/library/argparse.html) and writes values to a file that can be `source`d. 

# Scripts

`superserver` installs several convenience scripts: 

* `${SUPSERV_HOME_DIR}/list`: list the `systemd` services associated with this `superserver`
* `${SUPSERV_HOME_DIR}/test`: run a foreground test of the executable that will be run by each `systemd` service associated with this `superserver`
* `${SUPSERV_HOME_DIR}/check`: summarize how many `systemd` services associated with this `superserver` are currently active
* `${SUPSERV_HOME_DIR}/start`: (re-)start `systemd` services associated with this `superserver`
* `${SUPSERV_HOME_DIR}/stop`: stop running `systemd` services associated with this `superserver`
* `${SUPSERV_HOME_DIR}/remove`: stop and remove the `systemd` services
* `${SUPSERV_HOME_DIR}/clean`: stop and remove the `systemd` services _and_ delete all source and logs from `SUPSERV_HOME_DIR` and `SUPSERV_LOG_DIR`

# Tests

In discussing the following, we presume you first execute

```
superserver$ export SUPSERV_SUPSERV_DIR=${PWD}
```

to define the `superserver` core directory. 

There are tests you can explore in the `tests` folder. You can install, run, and clean up _all_ the tests with 

```
superserver/tests$ sudo -E ./install_all_tests.sh
... (edit apache configuration)
superserver/tests$ ./run_all_tests.sh
...
superserver/tests$ sudo -E ./clean_all_tests.sh
...
```

This is probably the quickest way to get going. 

Note though that we don't yet force `apache` reconfigurations and thus the load balancer needs to be set up manually if you haven't done so before. The install script will print out a configuration snippet to copy and paste into the appropriate config. The single script `alltests.sh` should work if your `apache` configuration _already has_ automatically replaceable sections for these tests. 

## node (express)

You should be able to run this test with the following commands: 

```
superserver/tests/node$ sudo -E ./${SUPSERV_SUPSERV_DIR}/superserver -n 3
```

(or another number if you like). To check that this worked, you should at least see the new services: 

```
$ /etc/basic-node-test/list
-rw-r--r-- 1 root root 537 Jun  7 13:38 /etc/systemd/system/basic-node-test-1.service
-rw-r--r-- 1 root root 537 Jun  7 13:38 /etc/systemd/system/basic-node-test-2.service
-rw-r--r-- 1 root root 537 Jun  7 13:38 /etc/systemd/system/basic-node-test-3.service
-rw-r--r-- 1 root root 457 Jun  7 13:38 /etc/systemd/system/basic-node-test.service
```

You can run a spot test independent of the `systemd` and `apache` with 

```
$ /etc/basic-node-test/test
```

Then you can edit and reload your `apache` and start the servers with 

```
$ sudo systemctl restart apache2.service
$ sudo systemctl restart superserver.service
```

These commands don't print any checks. You can verify this worked with the following: 

```
$ /etc/basic-node-test/check
superserver/basic-node-test: 3/3 process services currently active.
```

You can then "really" check the superserver with the following: 

```
$ for i in $( seq 1 10 ) ; do curl http://localhost/basic-node-test/path ; done
Hello World! Process 1 listening on port 5001 got call to /path
Hello World! Process 2 listening on port 5002 got call to /path
Hello World! Process 3 listening on port 5003 got call to /path
Hello World! Process 1 listening on port 5001 got call to /path
Hello World! Process 2 listening on port 5002 got call to /path
Hello World! Process 3 listening on port 5003 got call to /path
Hello World! Process 1 listening on port 5001 got call to /path
Hello World! Process 2 listening on port 5002 got call to /path
Hello World! Process 3 listening on port 5003 got call to /path
Hello World! Process 1 listening on port 5001 got call to /path
```

You can clean everything up with the following: 

```
$ sudo /etc/basic-node-tests/clean
$ ls -al /etc/systemd/system/basic-node-test*
ls: cannot access '/etc/systemd/system/basic-node-test*': No such file or directory
$ ls -al /etc/basic-node-test*
ls: cannot access '/etc/basic-node-test*': No such file or directory
```

## python (Flask)

You should be able to run this test with the following commands: 

```
superserver/tests/python$ sudo -E ./${SUPSERV_SUPSERV_DIR}/superserver -n 3
```

You can run all the same basic checks as above

```
$ /etc/basic-flask-test/list
... 
$ /etc/basic-flask-test/test
... 
```

and restart `apache` and the `superserver` and check with

```
$ sudo systemctl restart apache2.service
$ sudo systemctl restart basic-flask-test.service
$ /etc/basic-flask-test/check
superserver/basic-flask-test: 3/3 process services currently active.
```

You can then fully check the superserver install with the following: 

```
$ for i in $( seq 1 10 ) ; do curl http://localhost/basic-node-test/path ; done
Hello World! Process 1 listening on port 5001 got call to /path
Hello World! Process 2 listening on port 5002 got call to /path
Hello World! Process 3 listening on port 5003 got call to /path
Hello World! Process 1 listening on port 5001 got call to /path
Hello World! Process 2 listening on port 5002 got call to /path
Hello World! Process 3 listening on port 5003 got call to /path
Hello World! Process 1 listening on port 5001 got call to /path
Hello World! Process 2 listening on port 5002 got call to /path
Hello World! Process 3 listening on port 5003 got call to /path
Hello World! Process 1 listening on port 5001 got call to /path
```

and clean up with 

```
$ sudo /etc/basic-flask-test/clean
```

## golang (net/http)

You probably get the picture by now. But [go](https://golang.org/) provides an example where it isn't hard to show how `superserver` handles missing requirements. If you don't have `go` installed and configured, specifically if you don't have a `go` command and/or [`GOPATH`](https://github.com/golang/go/wiki/GOPATH) is not set in your environment [as is standard](https://github.com/golang/go/wiki/SettingGOPATH), the `tests/golang/install.sh` file will exit with an error and `superserver` with not install any of the services: 

```
superserver/tests/golang$ sudo -E ${SUPSERV_SUPSERV_DIR}/superserver -n 3

= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

  SUPERSERVER v0.1

= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

  Installing superserver: a wrapper service for simple apache load  
  balancing of other servers you want to run on your machine. 

... 
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

  local install failed. check

    /etc/basic-golang-test/install.err
    /etc/basic-golang-test/install.out

  for details.

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

  superserver install FAILED... see above for any available details.

= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
```

If, instead, you have `go` installed, this example should work as do those above. 

# Contact

[wrossmorrow@stanford.edu](mailto:wrossmorrow@stanford.edu)