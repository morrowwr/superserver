#!/usr/bin/env python
import os
import argparse

def a2e( a ) : 
	return str( a ).upper().replace('-','_')

if __name__ == "__main__" : 

	# CREATE a signal file
	# 
	# this is a flag for a "-h/--help" run: to the current directory
	# which we can use as a flag for install.sh to exit or continue
	with open( ".help" , "w" ) as f : 
		f.write( " " )

	parser = argparse.ArgumentParser()
	parser.add_argument( '-e' , '--env-var-file' , default=None )
	parser.add_argument( '-u' , '--unit-name'    , default=None )
	parser.add_argument( '-d' , '--home-dir'     , default=None )
	parser.add_argument( '-l' , '--log-dir'      , default=None )
	parser.add_argument( '-P' , '--base-port'    , default=None )
	parser.add_argument( '-n' , '--num-procs'    , default=None )
	parser.add_argument( '-b' , '--balancer'     , default=None )
	parser.add_argument( '-p' , '--location'     , default=None )
	parser.add_argument( '-w' , '--apache-conf'  , default=None )
	parser.add_argument( '-r' , '--source-root'  , default=None )
	parser.add_argument( '-s' , '--source'       , default=None )
	parser.add_argument( '-a' , '--archive'      , default=None )
	parser.add_argument( '-i' , '--install'      , default=None )
	parser.add_argument( '-x' , '--start-script' , default=None )
	parser.add_argument( '-k' , '--stop-script'  , default=None )
	parser.add_argument( '-t' , '--dry-run'      , default=False , action="store_true" )
	parser.add_argument( '-A' , '--keep-archive' , default=False , action="store_true" )
	parser.add_argument( '-q' , '--quiet'        , default=False , action="store_true" )
	args = parser.parse_args()

	# REMOVE the signal file if we get here (-h/--help ** not ** called)
	os.remove( ".help" )
	
	# write a ".clargs" file with (re-)defined options
	with open( ".clargs" , "w" ) as f :
		for arg in vars( args ) : 
			val = getattr( args , arg )
			if val is not None : 
				if type(val) == bool : 
					f.write( "SUPSERV_%s=%s\n" % ( a2e(arg) , "1" if val else "0" ) )
				elif str(val).count(' ') > 0 : 
					f.write( "SUPSERV_%s=\"%s\"\n" % ( a2e(arg) , val ) )
				else : 
					f.write( "SUPSERV_%s=%s\n" % ( a2e(arg) , val ) )


					